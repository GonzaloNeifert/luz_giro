#include "chip.h"

#define TICKRATE_HZ (1000)

#define PORT(n)        ((uint8_t) n)
#define PIN(n)        ((uint8_t) n)
#define GRUPO(n)    ((uint8_t) n)

#define MATCH(n)    ((uint8_t) n)

#define    PORT_PULS3    ((uint8_t) 1)
#define    PIN_PULS3        ((uint8_t) 9)
#define    PORT_PULS2    ((uint8_t) 0)
#define    PIN_PULS2        ((uint8_t) 9)
#define    PORT_PULS1    ((uint8_t) 0)
#define    PIN_PULS1       ((uint8_t) 8)

#define    PORT_LED1    ((uint8_t) 0)
#define    PIN_LED1        ((uint8_t) 14)

#define    PORT_LED3    ((uint8_t) 1)
#define    PIN_LED3        ((uint8_t) 12)

#define    PORT_LEDr    ((uint8_t) 5)
#define    PIN_LEDr       ((uint8_t) 0)
#define    PORT_LEDg    ((uint8_t) 5)
#define    PIN_LEDg       ((uint8_t) 1)
#define    PORT_LEDb    ((uint8_t) 5)
#define    PIN_LEDb       ((uint8_t) 2)

#define    PORT_LED2    ((uint8_t) 1)
#define    PIN_LED2        ((uint8_t) 11)

#define ENTRADA       ((unsigned int) 1)
#define SALIDA       ((unsigned int) 2)

unsigned int Estado;
unsigned int Cuenta_Personas = 0;

void Configuracion_IO (void);
void Configuracion_Timers (void);

void Configuracion_IO (void){

    Chip_GPIO_Init(LPC_GPIO_PORT);

    Chip_SCU_PinMux( GRUPO(1) , PIN(6) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );
    Chip_SCU_PinMux( GRUPO(2) , PIN(10) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );
    Chip_SCU_PinMux( GRUPO(2) , PIN(11) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );
    Chip_SCU_PinMux( GRUPO(2) , PIN(12) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );
    Chip_SCU_PinMux( GRUPO(2) , PIN(0) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );
    Chip_SCU_PinMux( GRUPO(2) , PIN(1) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );
    Chip_SCU_PinMux( GRUPO(2) , PIN(2) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );

    Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, PORT(PORT_PULS3), PIN(PIN_PULS3));
    Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, PORT(PORT_PULS2), PIN(PIN_PULS2));
    Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, PORT(PORT_PULS1), PIN(PIN_PULS1));

    Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, PORT(PORT_LED1), PIN(PIN_LED1));
    Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, PORT(PORT_LED2), PIN(PIN_LED2));
    Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, PORT(PORT_LED3), PIN(PIN_LED3));
     Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, PORT(PORT_LEDr), PIN(PIN_LEDr));
     Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, PORT(PORT_LEDg), PIN(PIN_LEDg));
     Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, PORT(PORT_LEDb), PIN(PIN_LEDb));

    Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(PORT_LED1), PIN(PIN_LED1), (bool) false);
    Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(PORT_LED2), PIN(PIN_LED2), (bool) false);
    Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(PORT_LED3), PIN(PIN_LED3), (bool) false);
    Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(PORT_LEDr), PIN(PIN_LEDr), (bool) false);
    Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(PORT_LEDg), PIN(PIN_LEDg), (bool) false);
    Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(PORT_LEDb), PIN(PIN_LEDb), (bool) false);
}


void Configuracion_Timers (void){

    Chip_TIMER_Init(LPC_TIMER0);
    Chip_TIMER_Reset(LPC_TIMER0);

    Chip_TIMER_SetMatch(LPC_TIMER0, MATCH(0), SystemCoreClock / 50);
    Chip_TIMER_MatchEnableInt(LPC_TIMER0, MATCH(0));
    Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, MATCH(0));
    Chip_TIMER_StopOnMatchDisable(LPC_TIMER0, MATCH(0));
    Chip_TIMER_Enable(LPC_TIMER0);

    Chip_TIMER_Init(LPC_TIMER1);
    Chip_TIMER_Reset(LPC_TIMER1);

    Chip_TIMER_SetMatch(LPC_TIMER1, MATCH(0), SystemCoreClock / 4);
    Chip_TIMER_MatchEnableInt(LPC_TIMER1, MATCH(0));
    Chip_TIMER_ResetOnMatchEnable(LPC_TIMER1, MATCH(0));
    Chip_TIMER_StopOnMatchDisable(LPC_TIMER1, MATCH(0));

    Chip_TIMER_Init(LPC_TIMER2);
        Chip_TIMER_Reset(LPC_TIMER2);

        Chip_TIMER_SetMatch(LPC_TIMER2, MATCH(0), SystemCoreClock / 10);
        Chip_TIMER_MatchEnableInt(LPC_TIMER2, MATCH(0));
        Chip_TIMER_ResetOnMatchEnable(LPC_TIMER2, MATCH(0));
        Chip_TIMER_StopOnMatchDisable(LPC_TIMER2, MATCH(0));


    NVIC_ClearPendingIRQ(TIMER0_IRQn);
    NVIC_EnableIRQ(TIMER0_IRQn);
    NVIC_ClearPendingIRQ(TIMER2_IRQn);
        NVIC_EnableIRQ(TIMER2_IRQn);

    NVIC_ClearPendingIRQ(TIMER1_IRQn);
    NVIC_EnableIRQ(TIMER1_IRQn);

}


void TIMER0_IRQHandler (void){

    static unsigned int Cuenta_Puls = 0;

    if (Chip_TIMER_MatchPending(LPC_TIMER0, MATCH(0)))
    {
        Chip_TIMER_ClearMatch(LPC_TIMER0, MATCH(0));

        if (Chip_GPIO_GetPinState (LPC_GPIO_PORT, PORT(PORT_PULS3) , PIN(PIN_PULS3) ) == 0)
            Cuenta_Puls ++;

        else{
            if (Cuenta_Puls != 0){
                if (Cuenta_Puls >= 10 && Cuenta_Puls < 40){ // Entro una persona.
                	Estado = ENTRADA;
                    Cuenta_Personas ++;
                    Chip_TIMER_Enable(LPC_TIMER1);
                }

                if (Cuenta_Puls >= 40 && Cuenta_Puls < 80){ // Salio una persona.
                	Estado = SALIDA;
                    Cuenta_Personas --;
                    Chip_TIMER_Enable(LPC_TIMER1);
                }

                Cuenta_Puls = 0;
            }
        }
    }
}



void TIMER1_IRQHandler (void)
{
    static unsigned int Cuenta_Interrupciones = 0;
    static unsigned int Cuenta_Ticks_Led = 0;

    if (Chip_TIMER_MatchPending(LPC_TIMER1, MATCH(0)))
    {
        Chip_TIMER_ClearMatch(LPC_TIMER1, MATCH(0));

        Cuenta_Interrupciones ++;

        if (Cuenta_Interrupciones > 20){

            Cuenta_Interrupciones = 0;
            Chip_TIMER_Reset(LPC_TIMER1);
            Chip_TIMER_Disable(LPC_TIMER1);
            Chip_GPIO_SetPinState (LPC_GPIO_PORT, PORT(PORT_LED2), PIN (PIN_LED2), (bool) false);
            return;
        }

        if (Estado == SALIDA){
        	Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, PORT(PORT_LED1), PIN(PIN_LED1));
        }

		if (Cuenta_Personas > 5 && Estado == ENTRADA){
            Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, PORT(PORT_LED2), PIN(PIN_LED2));
		}

        if (Cuenta_Personas <= 5 && Estado == ENTRADA){

            Cuenta_Ticks_Led ++;

            if (Cuenta_Ticks_Led >= 4){

                Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, PORT(PORT_LED2), PIN(PIN_LED2));
                Cuenta_Ticks_Led = 0;
            }
        }


    }
}


void TIMER2_IRQHandler (void)
{
    static unsigned int Cuenta_Interrupciones = 0;
    static unsigned int Cuenta_Ticks_Led = 0;

    if (Chip_TIMER_MatchPending(LPC_TIMER2, MATCH(0)))
    {
        Chip_TIMER_ClearMatch(LPC_TIMER2, MATCH(0));
    }

    if (Chip_GPIO_GetPinState (LPC_GPIO_PORT, PORT(PORT_PULS1) , PIN(PIN_PULS1) ) == 0)
    {
    	 Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, PORT(PORT_LEDb), PIN(PIN_LEDb));
    }


}


int main(void)
{
    SystemCoreClockUpdate();

    Configuracion_IO ();
    Configuracion_Timers ();

    while (1){

        __WFI();
    }
}

